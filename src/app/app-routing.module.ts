import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './auth/guards/login.guard';
import { NotFoundComponent } from './main/pages/not-found/not-found.component';

const routes: Routes = [
  //{path: 'dashboard', component: DashboardComponent, canActivate: [LoginGuard]},
  //{path: '**', component: NotFoundComponent}
  {path: 'main', loadChildren: () => import('./main/main.module').then((m)=>m.MainModule)},
  {path: '', redirectTo: 'main', pathMatch: 'full'}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
