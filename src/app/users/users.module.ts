import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatGridListModule} from '@angular/material/grid-list';

import { UsersRoutingModule } from './users-routing.module';
import { UpdateComponent } from './pages/update/update.component';
import { OthersComponent } from './pages/others/others.component';
import { BarraComponent } from '../main/pages/barra/barra.component';

import {MatCardModule} from '@angular/material/card';


@NgModule({
  declarations: [
    UpdateComponent,
    OthersComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    MatCardModule,
    MatGridListModule
  ]
})
export class UsersModule { }
