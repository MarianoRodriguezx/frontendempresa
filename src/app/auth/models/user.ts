export interface User {
    id: number,
    username: string,
    email: string,
    rol: number,
    password: string
}
