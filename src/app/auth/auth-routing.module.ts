import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../main/pages/dashboard/pages/dashboard.component';
import { LoggeadoGuard } from './guards/loggeado.guard';
import { LoginGuard } from './guards/login.guard';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  {path:'login', component: LoginComponent, canActivate: [LoggeadoGuard]},
  {path: 'dashboard', component: DashboardComponent, canActivate: [LoginGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
