import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { Login } from './models/login';
import { Token } from './models/token';
import { User } from './models/user';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router) { }

  url = environment.url
  auth = environment.auth


  login(user: Login): Observable<Token> {
    const ruta = this.url+this.auth+"login"

    return this.http.post<Token>(ruta, user).pipe(
      tap( val => {
        //Mandar llamar service
        this.setToken(val.token)
        //val.token
        this.datosUser().subscribe({
          next: (r) => [
            localStorage.setItem('username', r.username)
          ]
        })
      })
    )
  }

  datosUser(): Observable<User>{
    const ruta = this.url+this.auth+"user"
    return this.http.get<User>(ruta)
  }

  cerrarSesion(){

    const url = environment.url
    const auth = environment.auth

    const ruta = url+auth+"logout"

    this.http.get(ruta).subscribe({
      next: (r) => [
        localStorage.removeItem("token"),
        localStorage.removeItem("username"),
        localStorage.removeItem("rol"),
        this.router.navigateByUrl('/login')
      ]
    })
  }

  setToken(token: string){
    localStorage.setItem("token", token)
  }

  setDatos(username: string){
    localStorage.setItem("username", username)
  } 
}
