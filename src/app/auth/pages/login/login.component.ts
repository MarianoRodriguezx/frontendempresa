import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { LoginService } from '../../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  submitted=false;
  bt="enabled";
  errores=false;

  constructor(private formBuiler: FormBuilder, private dataService: LoginService, private router: Router) { }

  ngOnInit(): void {
    this.form=this.formBuiler.group({
      email: ['', [
        Validators.required, 
        Validators.email,
        Validators.minLength(10),
        //Validators.pattern('[a-z09._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')
      ]],
      password: ['', [
        Validators.required, 
        Validators.minLength(8)
      ]]
    })
  }

  get f(): {[key:string]: AbstractControl}{return this.form.controls;}

  onSubmit(): void{
    this.submitted = true;

    if(this.form.invalid)
    {
      return console.log("ERROR")
    }

    else{
      //const user = {email: this.f['email'].value, password: this.f['password'].value}
      //this.bt='disabled'
      //console.log("1")
      this.dataService.login(this.form.value).subscribe({
        next: (r) => [
          this.router.navigateByUrl('')
          /*
          this.dataService.setToken(r.token),

          this.dataService.datosUser().subscribe({
            next: (response) => [
              this.dataService.setDatos(response.username),
              
            ]
          })*/
        ],
        error: (e) => [console.error(e), this.errores = true],
        complete: () => []
      })

      

      /*this.dataService.login(user).subscribe(data => {
        console.log(data)
        this.dataService.setToken(data.token.token)
        this.router.navigateByUrl('/dashboard')
      })*/
    }
  }

}
