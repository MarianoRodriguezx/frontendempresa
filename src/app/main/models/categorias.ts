export interface Categorias {
    message: string;
    data: midata;
}

export interface midata{
    id: number;
    nombre: string;
    icono: string;
}