import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AsignarCategoriaComponent } from './pages/asignar-categoria/asignar-categoria.component';
import { AsignarVistaComponent } from './pages/asignar-vista/asignar-vista.component';
import { BarraComponent } from './pages/barra/barra.component';
import { CategoriasComponent } from './pages/categorias/categorias.component';
import { RolesComponent } from './pages/roles/roles.component';
import { VistasComponent } from './pages/vistas/vistas.component';

const routes: Routes = [
  //{path: '', component: BarraComponent},
  /* {path: 'asignar_categoria', component: AsignarCategoriaComponent},
  {path: 'desactivar_categoria', component: DesactivarCategoriaComponent} */

  {path: '', component: BarraComponent, 
    children: [
      {path: 'dashboard', loadChildren: () => import('./pages/dashboard/dashboard.module').then((m) => m.DashboardModule)},
      {path: 'categorias', component: CategoriasComponent},
      {path: 'vistas', component: VistasComponent},
      {path: 'asignar_vista', component: AsignarVistaComponent},
      {path: 'roles', component: RolesComponent}
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
