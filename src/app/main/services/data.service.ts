import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { VistasHijas } from '../models/vistas-hijas';
import { VistasPadres } from '../models/vistas-padres';
import { Rol } from '../models/rol';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  url = environment.url
  api = environment.api

  categorias(): Observable<VistasPadres>{
    //const ruta = this.url+this.api+"vh"

    const rutaprueba = "http://127.0.0.1:3333/api/v1/categorias"
    //const rutaprueba = "http://127.0.0.1:3333/categorias/"
    return this.http.get<VistasPadres>(rutaprueba)
  }

  categoria(): Observable<any>{
    //const ruta = this.url+this.api+"vh"

    const rutaprueba = "http://127.0.0.1:3333/api/v1/categorias"
    //const rutaprueba = "http://127.0.0.1:3333/categorias/"
    return this.http.get<any>(rutaprueba)
  }

  rol(): Observable<Rol>{
    const rutaprueba = "http://127.0.0.1:3333/auth/v1/rol_user"
    return this.http.get<Rol>(rutaprueba)
  }

  submenu(): Observable<VistasHijas>{
    const rutaprueba = "http://127.0.0.1:3333/api/v1/vistas"
    return this.http.get<VistasHijas>(rutaprueba)
  }
  /*vp(): Observable<VistasPadres>{
    const ruta = this.url+this.api+"vp"

    return this.http.get<VistasPadres>(ruta)
  }*/

  roles(): Observable<any>{

    const ruta = this.url+this.api+"roles"

    return this.http.get<any>(ruta)
  }

  modRoles(id: number): Observable<any>{
    const ruta = this.url+this.api+"roles/"+id

    return this.http.delete<any>(ruta)
  }

  vistas(): Observable<any>{

    const ruta = this.url+this.api+"vistas"

    return this.http.get<any>(ruta)
  }

  rolVistas(rol: number): Observable<any>{

    const ruta = this.url+this.api+"vistas/"+rol

    return this.http.get<any>(ruta)
  }

  desactivarVistas(id: number): Observable<any>{

    const ruta = this.url+this.api+"vistas/"+id

    return this.http.delete<any>(ruta)
  }

  InsertarCategorias(data: any): Observable<any>{
    const ruta = this.url+"api/v1/categorias"

    return this.http.post<any>(ruta, data)
  }

  Categorias(): Observable<any>{
    const ruta = this.url+"api/v1/categorias"

    return this.http.get<any>(ruta)
  }

  allCategories(): Observable<any>{
    const ruta = this.url+"api/v1/allIndex"

    return this.http.get<any>(ruta)
  }

  DesactivarCategorias(id: number): Observable<any>{
    const ruta = this.url+"api/v1/categorias/"+id

    return this.http.delete<any>(ruta)
  }

  InsertarVistasHijas(data: any): Observable<any>{
    const ruta = this.url+this.api+"vistas"

    return this.http.post<any>(ruta, data)
  }
}
