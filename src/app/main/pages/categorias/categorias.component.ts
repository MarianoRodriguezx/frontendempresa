import { Component, Inject, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { DataService } from '../../services/data.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


export interface categorias {
  id: number,
  nombre: string;
  icono: string;
  status: boolean;
}

let ELEMENT_DATA: categorias[] = [];

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'icono', 'status', 'opciones'];
  dataSource: any

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private data: DataService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getCategorias()
  }

  getCategorias()
  {
    this.data.allCategories().subscribe({
      next: (r) => [
        console.log(r),
        ELEMENT_DATA = r.data
      ],
      complete: () => [
        this.dataSource = new MatTableDataSource(ELEMENT_DATA)
      ]
    })
  }

  statusCategoria(id: number)
  {
    this.data.DesactivarCategorias(id).subscribe({
      next: (r) => [
        console.log("todo bien")
      ],
      error: (e) => [
        console.log("mal")
      ],
      complete: () => [
        this.getCategorias()
      ]
    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ActualizarCategoria, {
      width: '250px',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

@Component({
  selector: 'actualizacion-categoria',
  templateUrl: 'actualizacion-categoria.html',
})
export class ActualizarCategoria {
  constructor(
    public dialogRef: MatDialogRef<ActualizarCategoria>,
    @Inject(MAT_DIALOG_DATA) public data: categorias,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
