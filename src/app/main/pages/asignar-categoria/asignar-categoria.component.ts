import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-asignar-categoria',
  templateUrl: './asignar-categoria.component.html',
  styleUrls: ['./asignar-categoria.component.css']
})
export class AsignarCategoriaComponent implements OnInit {

  isLinear = true;
  categoria!: FormGroup;
  id_cat: number = 0;
  vista!: FormGroup;
  roles: any[] = [];
  rolesCL: any[] = []

  constructor(private _formBuilder: FormBuilder, private data: DataService, private router: Router) {}

  ngOnInit() {

    this.Obtenerroles()

    this.categoria = this._formBuilder.group({
      nombre: ['', Validators.required],
      icono: ['', Validators.required]
    });

    this.vista = this._formBuilder.group({
      nombre: ['', Validators.required],
      ruta: ['', Validators.required],
      icono: ['', Validators.required],
      rol: ['', []]
    });

  }

  enviardata(){
    

    if(this.categoria.invalid || this.vista.invalid)
    {
      console.log("muerete")
    }

    else
    {
      console.log("ya no quiero acudir al recuerdo te quiero aqui")
    }
  }

  Obtenerroles()
  {
    this.data.roles().subscribe({
      next: (r) => {
        console.log(r.data)
        this.roles=r.data
      }
    })
  }

  rolesC(info: boolean, rol: number){
    if(info){
      this.rolesCL.push(rol)
    }
    else{
      this.rolesCL = this.rolesCL.filter(item => item !== rol)
    }
  }

  get f(): {[key:string]: AbstractControl}{return this.vista.controls;}

  submit(){
    this.data.InsertarCategorias(this.categoria.value).subscribe({
      next: (r) => [
        console.log("cat: " + r.data),
        this.id_cat=r.data,

        this.rolesCL.forEach(element => {
          console.log(element)

          const datos = {
            nombre: this.f['nombre'].value,
            ruta: this.f['ruta'].value,
            icono: this.f['icono'].value,
            categoria: this.id_cat,
            rol: element
          }

          console.log(datos)

          this.data.InsertarVistasHijas(datos).subscribe({
            next: (r) => [
              console.log("yo intente salvar todo este amor con hielo y se murio")
            ],
            error: (e) => [console.log(e)],
            complete: () => [
              this.router.navigateByUrl("/main/dashboard")
            ]
          })
        })
      ],
      error: (e) => [console.log(e)]
    })
  }
}
