import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarVistaComponent } from './registrar-vista.component';

describe('RegistrarVistaComponent', () => {
  let component: RegistrarVistaComponent;
  let fixture: ComponentFixture<RegistrarVistaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarVistaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarVistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
