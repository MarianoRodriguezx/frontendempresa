import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-registrar-vista',
  templateUrl: './registrar-vista.component.html',
  styleUrls: ['./registrar-vista.component.css']
})
export class RegistrarVistaComponent implements OnInit {

  isLinear = false;
  categoria!: FormGroup;
  id_cat: number = 0;
  vista!: FormGroup;
  roles: any[] = [];
  rolesCL: any[] = []

  categorias: any[] = []

  constructor(private _formBuilder: FormBuilder, private data: DataService, private router: Router) { }

  ngOnInit(): void {

    this.Obtenerroles()
    this.ObtenerCategorias()

    this.vista = this._formBuilder.group({
      nombre: ['', Validators.required],
      ruta: ['', Validators.required],
      icono: ['', Validators.required],
      categoria: ['', Validators.required],
      rol: ['', []]
    });

  }

  Obtenerroles()
  {
    this.data.roles().subscribe({
      next: (r) => {
        console.log(r.data)
        this.roles=r.data
      }
    })
  }

  ObtenerCategorias()
  {
    this.data.categoria().subscribe({
      next: (r) => [
        console.log(r),
        this.categorias = r.data
      ]
    })
  }

  rolesC(info: boolean, rol: number){
    if(info){
      this.rolesCL.push(rol)
    }
    else{
      this.rolesCL = this.rolesCL.filter(item => item !== rol)
    }
  }

  get f(): {[key:string]: AbstractControl}{return this.vista.controls;}

  submit(){
        this.rolesCL.forEach(element => {
          console.log(element)

          const datos = {
            nombre: this.f['nombre'].value,
            ruta: this.f['ruta'].value,
            icono: this.f['icono'].value,
            categoria: this.f['categoria'].value,
            rol: element
          }

          console.log(datos)

          this.data.InsertarVistasHijas(datos).subscribe({
            next: (r) => [
              console.log("yo intente salvar todo este amor con hielo y se murio")
            ],
            error: (e) => [console.log(e)],
            complete: () => [
              this.router.navigateByUrl("/main/dashboard")
            ]
          })
        })
      
  }

}
