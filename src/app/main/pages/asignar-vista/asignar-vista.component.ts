import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

export interface roles {
  id: number,
  name: string;
  status: boolean;
}

let ELEMENT_DATA: roles[] = [];
let vistas: any[] = [];
let vistas_rol: any[] = [];
let rol: number = 0


@Component({
  selector: 'app-asignar-vista',
  templateUrl: './asignar-vista.component.html',
  styleUrls: ['./asignar-vista.component.css']
})
export class AsignarVistaComponent implements OnInit {

  

  displayedColumns: string[] = ['name','opciones'];
  dataSource: any

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private data: DataService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getRoles()
    
  }

  getRoles()
  {
    this.data.roles().subscribe({
      next: (r) => [
        console.log(r),
        ELEMENT_DATA = r.data
      ],
      complete: () => [
        this.dataSource = new MatTableDataSource(ELEMENT_DATA)
      ]
    })
  }

  openDialog(id: number): void {

    rol = id
    this.data.rolVistas(id).subscribe({
      next: (r) => [
        vistas_rol = r.data
      ]
    })

    this.data.vistas().subscribe({
      next: (r) => [
        vistas=r.data
      ]
    })

    const dialogRef = this.dialog.open(AsignarVista, {
      width: '250px',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}



@Component({
  selector: 'relacionar-vistas',
  templateUrl: 'relacionar-vistas.html',
})
export class AsignarVista {
  constructor(
    public dialogRef: MatDialogRef<AsignarVista>,
    @Inject(MAT_DIALOG_DATA) public data: roles, private _formBuilder: FormBuilder, private router: Router
  ) {}

  vista!: FormGroup;
  roles: any[] = [];
  rolesCL: any[] = []

  vistass = vistas
  vista_rol = vistas_rol

  rol = rol

  ngOnInit(): void {
    
    this.vista=this._formBuilder.group({
      vista: ['', []]
    })
  }

  rolesC(info: boolean, rol: number){
    if(info){
      this.rolesCL.push(rol)
    }
    else{
      this.rolesCL = this.rolesCL.filter(item => item !== rol)
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}


