import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarVistaComponent } from './asignar-vista.component';

describe('AsignarVistaComponent', () => {
  let component: AsignarVistaComponent;
  let fixture: ComponentFixture<AsignarVistaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignarVistaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarVistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
