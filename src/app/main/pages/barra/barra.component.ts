import {Component, OnInit, ViewChild } from '@angular/core';
import { LoginService } from 'src/app/auth/login.service';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-barra',
  templateUrl: './barra.component.html',
  styleUrls: ['./barra.component.css']
})
export class BarraComponent implements OnInit {

  
  panelOpenState = false;
  showFiller = false;
  constructor( private dataService: LoginService, private data: DataService) { }
  user = localStorage.getItem("username")

  loguer = localStorage.getItem("loguer")

  lista2: any
  lista: any
  listaC: any
  
  ngOnInit(): void {
    this.getMenu()
  }

  getMenu()
  {
    this.data.categorias().subscribe({
      next: (r) => [
        console.log(r.data),
        this.lista = r.data
      ],
      error: (e) => [console.error(e)]
    })
  }

  cerrar(){
    this.dataService.cerrarSesion()
  }

}
