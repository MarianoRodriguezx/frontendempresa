import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AsignarCategoriaComponent } from '../asignar-categoria/asignar-categoria.component';
import { BarraComponent } from '../barra/barra.component';
import { DashboardComponent } from './pages/dashboard.component';

const routes: Routes = [
  {path: '', component: DashboardComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
