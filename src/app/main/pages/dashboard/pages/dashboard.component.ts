import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/auth/login.service';
import { DataService } from 'src/app/main/services/data.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private dataService: LoginService, private data: DataService) { }

  username = localStorage.getItem("username")
  rol:any;
  rolDato: boolean = false;

  lista2: any
  lista: any
  listaC: any

  ngOnInit(): void {

   this.data.rol().subscribe({
      next: (r) => [
        console.log(r),
        this.rol = r
      ]
    })

    console.log(this.rol + "rp")

    if(this.rol==1){
      this.rolDato=true
    }
    //setInterval(()=> {
      
    
    this.data.categorias().subscribe({
      next: (r) => [
        console.log(r.data),
        console.log(r.data),
        this.lista = r.data,
      
        /*this.data.submenu().subscribe({
          next: (r) => [
            console.log(r.data),
            this.lista2=r.data,
            //this.checar()
          ]
        })
        //this.lista= r.data.dob.nombre*/
      ],
      error: (e) => [console.error(e)]
    })
    
    
  
  //}, 10000);
    /*this.data.vp().subscribe({
      next: (r) => [
        console.log(r.data),
        this.lista2 = r.data
      ],
      error: (e) => [console.error(e)]
    })*/
  }

  /*checar(){

    this.lista2.forEach((element: any) => {

      if(element.categoria == 1){
        console.log(element.nombre)
        this.listaC.push(element.nombre)
      }

      //console.log(element.ruta)
    });
    
  }*/

  cerrar(){
    this.dataService.cerrarSesion()
  }

}