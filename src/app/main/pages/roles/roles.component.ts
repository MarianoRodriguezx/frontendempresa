import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { DataService } from '../../services/data.service';

export interface roles {
  id: number,
  name: string;
  status: boolean;
}

let ELEMENT_DATA: roles[] = [];

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  displayedColumns: string[] = ['nombre','status', 'opciones'];
  dataSource: any

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private data: DataService) { }

  ngOnInit(): void {
    this.getRoles()
  }

  getRoles()
  {
    this.data.roles().subscribe({
      next: (r) => [
        ELEMENT_DATA = r.data
      ],
      complete: () => [
        this.dataSource = new MatTableDataSource(ELEMENT_DATA)
      ]
    })
  }

  modRoles(id: number)
  {
    this.data.modRoles(id).subscribe({
      next: (r) => [

      ],
      complete: () => [
        this.getRoles()
      ]
    })
  }
}
