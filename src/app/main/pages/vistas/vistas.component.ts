import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import { DataService } from '../../services/data.service';

export interface categorias {
  id: number,
  nombre: string;
  ruta: string;
  icono: string;
  categoria: number;
  rol: number;
  status: boolean;
  cats: any;
}

let ELEMENT_DATA: categorias[] = [];

@Component({
  selector: 'app-vistas',
  templateUrl: './vistas.component.html',
  styleUrls: ['./vistas.component.css']
})
export class VistasComponent implements OnInit {

  displayedColumns: string[] = ['categoria','nombre', 'ruta', 'icono', 'status', 'opciones'];
  dataSource: any

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private data: DataService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getVistas()
  }

  getVistas()
  {
    this.data.vistas().subscribe({
      next: (r) => [
        console.log(r),
        ELEMENT_DATA = r.data
      ],
      complete: () => [
        this.dataSource = new MatTableDataSource(ELEMENT_DATA)
      ]
    })
  }

  statusVista(id: number)
  {
    this.data.desactivarVistas(id).subscribe({
      next: (r) => [
        console.log(r)
      ],
      error: (e) => [],
      complete: () => [this.getVistas()]
    })
  }

}
