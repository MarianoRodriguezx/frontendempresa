import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';



import { MainRoutingModule } from './main-routing.module';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { BarraComponent } from './pages/barra/barra.component';

import { ReportesComponent } from '../reportes/pages/reportes/reportes.component'; 
import { AsignarCategoriaComponent } from './pages/asignar-categoria/asignar-categoria.component';
import { CategoriasComponent } from './pages/categorias/categorias.component';
import { VistasComponent } from './pages/vistas/vistas.component';
import { RegistrarVistaComponent } from './pages/registrar-vista/registrar-vista.component';
import { AsignarVista, AsignarVistaComponent } from './pages/asignar-vista/asignar-vista.component';
import { RolesComponent } from './pages/roles/roles.component';


@NgModule({
  declarations: [
    NotFoundComponent,
    BarraComponent,
    ReportesComponent,
    AsignarCategoriaComponent,
    CategoriasComponent,
    VistasComponent,
    RegistrarVistaComponent,
    AsignarVistaComponent,
    AsignarVista,
    RolesComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatCheckboxModule,
    MatCardModule,
    MatToolbarModule,
    MatMenuModule,
    MatGridListModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    ReactiveFormsModule,
    MatTableModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatDialogModule,
    MatSelectModule
  ],

  exports:[

  ]
})
export class MainModule { }
