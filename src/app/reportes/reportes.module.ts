import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';


import { ReportesRoutingModule } from './reportes-routing.module';
import { ReportesComponent } from './pages/reportes/reportes.component';


@NgModule({
  declarations: [
    //ReportesComponent
  ],
  imports: [
    CommonModule,
    ReportesRoutingModule,
    MatCardModule,
    MatButtonModule
  ]
})
export class ReportesModule { }
